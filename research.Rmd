---
title: "Research"
description: |
  Health, methodology, education, wine, random.
---

# Health Psychology



In 2022, I started a new exciting project promoting the use of ebikes as a means of both exercise and inclusive and sustainable transport. This is fundeded by the [Sustainable Energy Authority of Ireland](www.seai.ie) and the (Irish) [Department of Transport](https://www.gov.ie/en/organisation/department-of-transport/). More information will soon be available on the [ISCycle website](www.iscycle.ie)

There's a particular nice sequence of work led by my former PhD studnt Anna Widayanti:

* [Health-seeking behavior of people in Indonesia: A narrative review](https://doi.org/10/ghc9s6)
* [Effectiveness of an intensive community-based intervention for people with type 2 diabetes in Indonesia: A pilot study](https://doi.org/10/gnszs9)
* [Lay perceptions and illness experiences of people with type 2 diabetes in Indonesia: A qualitative study](https://doi.org/10/ghc9s7)
* [Is expanding service through an outreach programme enough to improve immunisation uptake? A qualitative study in Indonesia](https://doi.org/10.1080/17441692.2020.1751229)
* [Medicine taking behaviours of people with type 2 diabetes in Indonesia: A qualitative study](https://doi.org/10.1007/s11096-019-00933-0)

Since 2006, I have been principally working in the area of helath psychology, with a range of quantitative and qualitative projects espeically involving medicines. I explore decision-making about medicines, including adherence, preventive health behaviours, and how culture influences medicine taking behaviour and understanding. I examine communication between healthcare professionals, and between healthcare professionals and patients. 



# Methodology

Since the first year of my PhD, I have been involved in teaching research methods and statistics. I'm an enthusiastic supporter of iniativies to promote openness and transparency in research, and am always trying upskill myself and others, and improve my methods.

I've recently published a [tutorial](https://www.tandfonline.com/doi/full/10.1080/21642850.2021.1920416) paper on why and how more poeple should use count regressions, for data that are highly skewed or contain a lot of zeros. This type of natural count data (be it number of cigarettes smoked, gym visits, blood donations), where many people might do the behaviour never, and some people do it a lot, is very common in health psychology. It also has a small companion [*R* package](https://cran.r-project.org/package=simfit).

<aside>
* Green, J. A. (2021). [Too many zeros and/or highly skewed? A tutorial on modelling health behaviour as count data with Poisson and negative binomial regression](https://doi.org/10.1080/21642850.2021.1920416). *Health Psychology and Behavioral Medicine*, 9(1), 436–455. 
</aside>

In my education section below, you can also read about my masters research looking at innovation in research methods teaching. 

# Education

I have a strong interest in **admissions** processes and predictors of academic success, initially as part of a role on a pharmacy admissions committee trying to increase the number of Maori students gain entry. I have reviewed a good number of papers in this area for *Medical Education* and *Advances in Health Sciences Education*.

<aside>
* Green, J.A., (2015). [The effect of English proficiency and ethnicity on academic performance and progress](https://doi.org/10.1007/s10459-014-9523-7). *Advances in Health Sciences Education*, 20, 219-228.  
</aside>

I also have been involved in several **interprofessional education** projects, and currently teach an interprofessional cohort in allied health. 

For my *Master of Arts in Teaching Learning and Scholarship*, I've also been blending my interest in methods with education. I'm a long time teacher of research methods and/or statistics, mostly to postgraduate professional cohorts. My project, which will be highlighted here soon, is looking at **innovativion in research methods teaching**. 

# Wine

No longer such an active area for me [sadface], but through a long collaboration with Wendy Parr (Lincoln University, Marlborough Wine Research Centre) and a number of her French collaborators (Dominique Valentin and others), we demonstrated that wine experts have a clear cognitive prototypes that influence their perceptions. We have also delineated sensory and physicochemical differences between different national and sub-national wine regions for Sauvignon blanc.

The last project I worked on here was looking to see whether the position of the moon per the biodynamic calendar influenced perceptions of wine. It did not [mic drop]

* Parr, W. V., Valentin, D., Reedman, P., Grose, C., & Green, J. A. (2017). Expectation or Sensorial Reality? An Empirical Investigation of the Biodynamic Calendar for Wine Drinkers. PloS one, 12, e0169257. https://doi.org/10.1371/journal.pone.0169257 \doi{10.1371/journal.pone.0169257}

I narrowly missed a large scale Marsden Fund grant in this area in 2005, which had it been successful, might have led to me having a very different career!

# Random

My PhD (2004) was located at the intersection of social psychology and sociolinguistics, but even back then, I had rather broad interests. I've had an extended but peripheral involvement in [evaluating different accents of English](accents.html), for example. And have a stack of file-drawered studies on the reasons analysis effect — listing your reasons underlying a judgement messes with your judgement. One of these is published, showing a carryover effect from reasoning to subsequent judgements. 
